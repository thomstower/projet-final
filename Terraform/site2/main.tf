provider "aws" {
    region     = "${var.AWS_REGION}"
    access_key = "AKIAXXXNTM5FJPQNUUTA"
    secret_key = "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"
}

resource "aws_key_pair" "ec2_terraform" {
    key_name   = "root@ops"
    public_key = file("/root/projet_final/Terraform/site2/keys_server.pub")
}

resource "aws_security_group" "subnet" {
  name = "Security-Groups"
  vpc_id = "vpc-1835257f"
    ingress {
        from_port = 22
        to_port = 22
        protocol = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }

egress {
        from_port       = 0
        to_port         = 0
        protocol        = "-1"
        cidr_blocks     = ["0.0.0.0/0"]
    }

ingress {
        from_port   = 80
        to_port     = 80
        protocol    = "tcp"
        cidr_blocks = ["0.0.0.0/0"]
    }
}

resource "aws_instance" "aws_terraform" {
	
	key_name      = aws_key_pair.ec2_terraform.key_name
 	ami	      = "ami-0b7dcd6e6fd797935"
 	instance_type = "t3.large"
	vpc_security_group_ids = [aws_security_group.subnet.id]

	tags = {
                Name = "Jekyll-server"
        }

connection { 
        type        = "ssh"
        user        = "ubuntu"
        private_key = file("/root/projet_final/Terraform/site2/keys_server")
        host        = self.public_ip
     	} 

user_data = <<-EOF

    #!/bin/bash
    sudo apt update
    sudo echo -e 'winner' | passwd root
    sudo sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/g' /etc/ssh/sshd_config

 EOF


provisioner "local-exec" {
 	working_dir= "/tmp"
        command = "echo '${aws_instance.aws_terraform.public_ip}' > /ip_address.txt"
	}
}

output "public_ip" {
        value = aws_instance.aws_terraform.public_ip
        }

